Vue.component('item1', {
    template:
        '<div data-tpl="item1" :class="body">' +
        '    <input v-model="msg" :class="in_text">' +
        '    <button v-on:click="notify" :class="btn">' +
        '    change value to {{msg}}' +
        '    </button>'+
        '</div>',
    props: ['msg'],
    data: function() {
        return {
            //Style
            root: 'col-lg-12',
            body: 'form-group col-xs-4',
            in_text: 'form-control',
            btn: 'btn btn-primary'
        }
    },
    methods: {
        'notify': function() {
            console.log('test');
        }
    }
})
