/**
 * According to vue.js build html form.
 * meno: 本範例的樣式是用bootstarp style產生對應的HTML表單及元素，
 *       並且根據傳入的物件產生相對應的表單元素。
 *       目前支援產生input's text, input's checkbox, select menu三種元素。
 * @param {object} info
 */
Vue.component('free-form', {
    template:
'   <div data-tpl="free-form">' +
'     <div class="container-fluid">' +
'        <form class="form-horizontal" name="{{info.id}}" id="{{info.id}}" v-on:submit.prevent>'+
'            <div :class="changeClass(item.status)" '+
'                 v-for="item in info.form" track-by="$index">'+
'                <div v-if="item.el !== \'btn\'" ' +
'                     class="form-group">' +
'                <label class="col-sm-2" for="{{item.title}}">{{item.title}}</label>'+
'                <div class="col-sm-7">'+
'                    <div v-if="item.el === \'checkbox\'">' +
'                      <label class="form-check-label" ' +
'                            v-for="(i, v) in item.val">' +
'                      <input type="checkbox" class="form-check-input" v-model="v">' +
'                      {{i}}' +
'                      </label>' +
'                    </div>' +
'                    <div v-if="item.el === \'input\'">' +
'                    <input class="form-control" placeholder="{{item.placeholder}}" ' +
'                           type="{{item.type}}" name="{{item.key}}" v-model=item.val' +
'                           id="{{item.title}}">'+
'                    </div>' +
'                  <select v-if="item.el === \'select\'" v-model="item.init" ' +
'                          class="form-control" name="{{item.key}}" >'+
'                    <option v-for="opt in item.val" v-bind:value="opt">{{opt}}</option>'+
'                  </select>'+
'                  <label v-if="item.el === \'radio\'" ' +
'                         v-for="i in item.val" ' +
'                         class="radio-inline" for="{{i}}">' +
'                    <input type="radio" name="{{i}}" value="{{i}}" v-model="item.init">' +
'                    {{i}}' +
'                  </label>' +
                  //According to status change css style.
'                  <span v-if="item.status === \'success\'" ' +
'                        class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>'+
'                  <span v-if="item.status === \'success\'" ' +
'                        class="sr-only">(success)</span>'+
'                  <span v-if="item.status === \'warning\'" ' +
'                        class="glyphicon glyphicon-warning-sign form-control-feedback" aria-hidden="true"></span>'+
'                  <span v-if="item.status === \'warning\'" ' +
'                        class="sr-only">(warning)</span>'+
'                  <span v-if="item.status === \'error\'" ' +
'                        class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'+
'                  <span v-if="item.status === \'error\'" ' +
'                        class="sr-only">(remove)</span>'+
'                </div>'+
'                </div>' +
'                <div class="col-sm-offset-5" v-if="item.el === \'btn\'">'+
'                  <button type="{{item.type}}" class="btn btn-primary" ' +
'                          @click="submit">{{item.val}}</button>'+
'                </div>'+
'            </div>'+
'        </form>'+
'     </div>' +
'   </div>',
    props: ['info'],//Receive object from parent component.
    data: function() {
        return {}
    },
    methods: {
        //Change different css style.
        changeClass: function(status) {
            var base = 'form-group';
            switch(status) {
                case 'success':
                    base += ' has-feedback has-success';
                break;
                case 'warning':
                    base += ' has-feedback has-warning';
                break;
                case 'error':
                    base += ' has-feedback has-error';
                break;
                default:
                    return base;
            }//endswitch
            return base;
        }
    }
})
