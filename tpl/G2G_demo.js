Vue.component('G2G_demo', {
    template:
    '<div data-tpl="G2G_demo" :class=root>' +
    '  <form class="form-horizontal">'+
    '    <div class="form-check">' +
    '        <label class="form-check-label" v-for="(index, item) in obj.checkList">' +
    '        <input type="checkbox" value={{item.name}}' +
    '               v-model=switchCheck class="form-check-input">{{item.name}}</label>' +
    '    </div>' +
    '  </form>' +
    '  <div>' +
    '  <!-- Nav tabs -->' +
    '  <ul class="nav nav-tabs" role="tablist">' +
    '    <li v-for="item in obj.checkList" v-if=item.checked ' +
    '        :class="getActiveStyle(item.name)" ' +
    '        role="presentation">' +
    '      <a href="#" data-toggle="tab" ' +
    '         @click="switchPage">{{item.name}}</a>' +
    '    </li>' +
    '  </ul>' +
    '  <div class="tab-content">' +
    '    <free-form :info="dataObj"></free-form>' +
    '  </div>' +
    '  </div>' +
    '</div>',
    data: function() {
        return {
            //All data
            obj: {
                activeTag: '',//Current active compnent.
                //Generator checkbox's number from server site.
                checkList: [
                    {id: 'g1', name: '非追不可', checked: false, active: false,
                     description: '', form: []},
                    {id: 'g2', name: '非登不可', checked: false, active: false,
                     description: '', form: []},
                    {id: 'g3', name: '邊境控管', checked: false, active: false,
                     description: '', form: []},
                    {id: 'g4', name: '水產驗證', checked: false, active: false,
                     description: '', form: []},
                    {id: 'g5', name: '原料驗證', checked: false, active: false,
                     description: '', form: []},
                    {id: 'FreeForm', name: 'FreeFrom', checked: false, active: false,
                     description: '', form: []},
                ]
            },
            share: [
                {el: 'input', key: 0, val: '我是共用欄位'},
                {el: 'select', key: 1, init: '我是預設值'},
                {el: 'checkbox', key: 2, val: {'001': false, '002': false, '003': false}}
            ],
            dataObj: {}
        }
    },
    ready: function() {
        var rand = 0;
        for (var key in this.obj.checkList) {
            rand = Math.floor((Math.random() * 10) + 1);
            this.obj.checkList[key].form = this.loopData(rand);
        }
    },
    methods: {
        switchPage: function() {
            this.switchActive = event.target.innerText;
        },//End of switchPage().
        getActiveStyle: function(item) {
            //According to array's item to set tab style(active or null).
            if (this.obj.activeTag === item) return 'active';
            else return '';
        },//End of getActiveStyle().
        pageLoad: function(child) {
            var vm = this;
            var id = '#mainContent';
            vm.funIndex = child;
            /*
            $(id).oLoader({
                backgroundColor: 'black',
                fadeInTime: 500,
                fadeOutTime: 1000,
                fadeLevel: 0.5
            });
            setTimeout(function() {
                $(id).oLoader('hide');
                vm.funIndex = child;
            },
            3000);
            */
        },
        loopData: function(count) {
            var rand = 0;
            var el = ['input', 'select', 'checkbox', 'radio'];
            var arr = [];
            var obj ={};
            for (var i = 0; i < count; i++) {
                rand = Math.floor(Math.random() * 4);
                radioiRand = Math.floor(Math.random() * 40000);
                obj = {
                    'input': {
                        key: i,
                        title: ('統一編號' + i),
                        el: 'input',
                        type: 'text',
                        val: ('測試用資料' + i),
                        placeholder: '',
                        status: ''
                    },
                    'select': {
                        key: i,
                        title: ('下拉式選單' + i),
                        el: 'select',
                        init: '第一個元素',
                        val: ['第一個元素', '我是預設值', '我是選取值'],
                        status: ''
                    },
                    'checkbox': {
                        key: i,
                        title: 'checkbox list',
                        el: 'checkbox',
                        val: {'001': true, '002': false, '003': false},
                        status: ''
                    },
                    'radio': {
                        key: i,
                        title: 'radio list',
                        el: 'radio',
                        init: 'radio測試1-' + radioiRand,
                        val: ['radio測試1-' + radioiRand,
                              'radio測試2-' + radioiRand + 5 ,
                              'radio測試3-' + radioiRand + rand],
                        status: ''
                    }
                };
                if (i === 3 && el[rand] === 'radio') {
                    if (this.share.length <= 3) {
                        this.share.push(
                            {el: 'radio', key: 3, init: 'ok3'}
                        );
                    }//fi
                    obj['radio'].init = 'ok3';
                    obj['radio'].val = ['ok1', 'ok2', 'ok3'];
                }//fi
                arr.push(obj[el[rand]]);
            }
            return arr;
        },
        maintainShare: function(obj1, obj2) {
            for (var index in obj2) {
                for (var key in obj1) {
                    if (obj1[key].key === obj2[index].key &&
                        obj1[key].el === obj2[index].el)
                    {
                        switch (obj2[key].el) {
                            case 'input':
                                obj1[key].val = obj2[index].val;
                            break;
                            case 'select':
                                obj1[key].init = obj2[index].init;
                            break;
                            case 'checkbox':
                                this.maintainShare(obj1[key].val, obj2[index].val);
                            break;
                            case 'radio':
                                obj1[key].init = obj2[index].init;
                            break;
                            default:
                                obj1[key] = obj2[index];
                        }//endswitch
                    }//fi
                }//endfor
            }//endfor
        }
    },
    computed: {
        switchCheck: {
            get: function() {
                return;
            },
            set: function(val) {
                var checks = 0;
                var tagName = event.target.value;
                var originIndex = 0;
                for (row in this.obj.checkList) {
                    //If name equivalent checkList's name.
                    if (this.obj.checkList[row].name === tagName) {
                        originIndex = row;
                        this.obj.checkList[row].checked = val;
                    }//fi
                    if (this.obj.checkList[row].checked) checks += 1;
                }//endfor
                if (val) {
                    if (checks === 1) {
                        this.switchActive = tagName;
                    }//fi
                } else {
                    var flag = true;
                    if (this.switchActive === tagName) {
                        for (var i = originIndex; i > -1; i--) {
                            if (this.obj.checkList[i].checked) {
                                this.switchActive = this.obj.checkList[i].name;
                                flag = false;
                                break;
                            }
                        }//endfor
                        if (flag) {
                            for (var i = originIndex; i < this.obj.checkList.length; i++) {
                                if (this.obj.checkList[i].checked) {
                                    this.switchActive = this.obj.checkList[i].name;
                                    flag = false;
                                    break;
                                }
                            }//endfor
                        }//fi
                        if (flag) {
                            this.switchActive = '';
                        }//fi
                    }//fi
                }//fi
            }
        },
        switchActive: {
            get: function() {
                //console.log(this.obj.activeTag);
                return this.obj.activeTag;
            },
            set: function(val) {
                this.maintainShare(this.share, this.dataObj.form);
                if (val === '') {
                    this.dataObj = {};
                } else {
                    //Seach id from checkList object.
                    for (row in this.obj.checkList) {
                        //If name equivalent checkList's name.
                        if (this.obj.checkList[row].name === val) {
                            this.maintainShare(this.obj.checkList[row].form, this.share);
                            this.dataObj = this.obj.checkList[row];
                            //console.log(this.dataObj);
                        }//fi
                    }//endfor
                }//fi
                //Set actice name to child dataObj and current component tab name.
                this.obj.activeTag = val;
            }
        }//End of switchActive.
    }
});
