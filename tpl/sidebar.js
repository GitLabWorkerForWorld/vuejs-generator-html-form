Vue.component('sidebar', {
    template:
    '<div id="sidebar" data-tpl="sidebar" v-bind:class="root">' +
    '    <div v-bind:class="body">' +
    '       <ul v-bind:class="ul" id="side-menu">' +
    '           <li><a @click="switchPage" href="#">G2G_demo</a></li>' +
    '       </ul>' +
    '    </div>' +
    '</div>',
    data: function() {
        return {
            //Set element css class name.
            root: 'sidebar',//tab style
            body: 'sidebar-nav navbar-collapse',//navbar style
            ul: 'nav',//ul style
            item: '',//item style
            item_on: '',//item_on style
            icon: ''
        }
    },
    methods: {
        switchPage: function() {
            this.$emit('switch-page', event.target.innerText);
            //Change page view components via user trigger event.
            //this.child = event.target.innerText;
        }
    },
    ready : function() {
        //Use metisMenu() to build sidebar.
        $('#sidevbar').metisMenu();
    }
})
